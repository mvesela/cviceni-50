const { renameCviceni } = require('./rename-cviceni-function');

// Command line arguments
const oldNameIndex = process.argv.indexOf('--oldname');
const newNameIndex = process.argv.indexOf('--newname');

// Setup names
const oldName = process.argv[oldNameIndex + 1];
const newName = process.argv[newNameIndex + 1];

renameCviceni(oldName, newName).then(
  (result) => {
    if (result) console.log('=======================CVICENI-WAS-SUCCESSFULLY-RENAMED=======================');
    else console.log('=======================CVICENI-WAS-NOT-RENAMED=======================');
  },
).catch();
